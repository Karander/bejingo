class Clip < ApplicationRecord
    belongs_to :owner, polymorphic: true
    default_scope -> { order(created_at: :desc) }
    validates :adress, presence: true, uniqueness: { scope: [:owner_type, :owner_id] }
    before_save { self.adress=adress.split("=").last }
    validates :owner, presence: true

end
