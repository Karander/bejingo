class Tag < ApplicationRecord
    before_save { self.name=name.downcase }
    has_many :taggers, dependent: :destroy
    has_many :albums, through: :taggers
    validates :name, presence: true, length: {maximum: 15, minimum: 3}, uniqueness: { case_sensitive: false }
end
