class Tagger < ApplicationRecord
  belongs_to :album
  belongs_to :tag
  validates :album_id, presence: true, uniqueness: { scope: :tag_id }
  validates :tag_id, presence: true
end
