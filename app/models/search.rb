class Search < ApplicationRecord
    
    def search_albums
        albums = Album.all
        
        albums = albums.where("title LIKE ?", "%#{name}%") if name.present?
        albums = albums.joins(:tags).where("name LIKE ?", tag) if tag.present?
        if min_likes.present?
            stack=[]
            albums.each do |album|
                stack.push(album) if (album.affections.where(value: 1).count >= min_likes)
            end
            albums = stack
        end
        
        return albums
    end
    
end
