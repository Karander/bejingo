class Affection < ApplicationRecord
  belongs_to :user
  belongs_to :album
  validates :value, presence: true, numericality: { only_integer: true }
  validates :album_id, presence: true
  validates :user_id, presence: true
  validate :correct_value
  
  def correct_value
    unless value==-1 || value==1
      errors.add(:value, "Inproper value for an affection")
    end
  end
  
end
