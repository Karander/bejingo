class Album < ApplicationRecord
  belongs_to :user
  has_many :clips, as: :owner, dependent: :destroy
  has_many :users, through: :affections
  has_many :affections, dependent: :destroy
  has_many :taggers, dependent: :destroy
  has_many :tags, through: :taggers
  validates :title, presence: true
  validates :user_id, presence: true
  
end
