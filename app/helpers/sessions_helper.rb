module SessionsHelper
    
    def log_in(user)
        session[:user_id]=user.id
    end
    
    def current_user
        if (user_id = session[:user_id])
            @current_user ||= User.find_by(id: user_id)
        elsif (user_id = cookies.signed[:user_id])
            user=User.find_by(id: user_id)
            if user && user.authenticated?(cookies[:remember_token])
                log_in(user)
                @current_user=user
            end
        end
    end
    
    def logged_in?
        !current_user.nil?
    end
    
    def log_out
        forget(current_user)
        session.delete(:user_id)
        @current_user=nil
    end
    
    def remember(user)
        user.remember
        cookies.permanent.signed[:user_id]=user.id
        cookies.permanent[:remember_token]=user.remember_token
    end
    
    def forget(user)
        user.forget
        cookies.delete(:user_id)
        cookies.delete(:remember_token)
    end
    
    def current_user?(user)
        user==current_user
    end
    
    def store_location
       session[:forwarding_url] = request.original_url if request.get? 
    end
    
    def redirect_back_or(default)
       redirect_to(session[:forwarding_url] || default)
       session.delete(:forwarding_url)
    end
    
## edit post helpers ##    
    
    def get_post(item)
        session[:post_to_edit] = item
    end
    
    def post_to_edit
        @post_to_edit = session[:post_to_edit]
    end
    
    def post_end_edit
        session.delete(:post_to_edit)
        @post_to_edit=nil
    end
    
## display video helpers ##
    
    def get_clip(clip_adress, clip_desc=" ")
        session[:clip_adress]=clip_adress
        session[:clip_desc]=clip_desc
    end
    
    def clip_to_display
        @clip_a=session[:clip_adress]
    end
    
    def desc_to_display
        @clip_d=session[:clip_desc]
    end
    
    def clip_end_display
        session.delete(:clip_adress)
        session.delete(:clip_desc)
        @clip_a=nil
        @clip_d=nil
    end
    
## display album helpers ##

    def get_album(id)
        session[:album]=id
    end
    
    def album_to_display
        @album=session[:album]
    end
    
    def album_end_display
        session.delete(:album)
        @album=nil
    end
    
## playlist helpers ##

    def create_playlist(clips)
        list=[]
        clips.each do |clip|
            list.push(clip.id)
        end
        session[:playlist]=list
    end
    
    def playlist
        @playlist=session[:playlist]
    end
    
    def playlist_start
        session[:play_item]=0
    end
    
    def play_next
        session[:play_item]+=1
    end
    
    def play_item(item)
        session[:play_item]=item
    end
    
    def item_to_play
        @item=session[:play_item]
    end
    
    def playlist_end
        session.delete(:playlist)
        session.delete(:play_item)
        @item=nil
        @playlist=nil
    end
    
    def autoplay(value)
        cookies[:autoplay]=value
    end
    
    def toggle_autoplay
        if cookies[:autoplay]=="yes"
            autoplay("no")
        else
            autoplay("yes")
        end
    end
        
end
