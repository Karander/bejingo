module ApplicationHelper
    
    #Returns the title of the page.
    def full_title(page_title="")
        base_title = "bejingo"
        if page_title.empty?
            base_title
        else
            page_title + " | " + base_title
        end
    end
    
end
