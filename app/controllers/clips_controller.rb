class ClipsController < ApplicationController
    
    before_action :logged_in_user, only: [:new, :destroy, :edit, :update, :create]
    before_action :correct_user, only: [:destroy, :edit, :update]
    
    def show
        @clip=Clip.find(params[:id])
        clip_end_display
        get_clip(@clip.adress, @clip.description)
        
        respond_to do |format|
            format.html {
                if album_to_display.nil?
                    redirect_to clips_user_path(@clip.owner_id)
                else
                    play_item(playlist.index(params[:id].to_i))
                    redirect_to albums_user_path(@clip.owner.user_id)
                end }
            format.js
        end
    end
    
    def new
        if album_to_display.nil?
            @clip=current_user.clips.build
        else
            @clip=Album.find(album_to_display).clips.build
        end
    end
    
    def destroy
        @clip.destroy
        flash[:success]="Clip deleted"
        redirect_to request.referrer || clips_user_path(current_user)
    end
    
    def edit
        @clip=current_user.clips.find_by(id: params[:id])
    end
    
    def update
        @clip=current_user.clips.find_by(id: params[:id])
        if @clip.update_attributes(update_params)
            flash[:success]="Clip updated"
            get_clip(@clip.adress, @clip.description)
            redirect_to clips_user_path(current_user)
        else
            render "edit"
        end
    end
    
    def create
        adress=params[:clip][:adress]
        adress=adress.split("=").last
        if album_to_display.nil?
            @clip=current_user.clips.build(clip_params)
            @dupl=current_user.clips.find_by(adress: adress)
        else
            @clip=Album.find(album_to_display).clips.build(clip_params)
            @dupl=Album.find(album_to_display).clips.find_by(adress: adress)
        end
        if @dupl==nil and @clip.save
            flash[:success]="Clip added!"
            if album_to_display.nil?
                redirect_to clips_user_path(current_user)
            else
                redirect_to albums_user_path(current_user)
            end
        else
            unless @dupl==nil
                flash.now[:danger]="Clip allready existing"
            end
            render 'new'
        end
    end
    
private
    
    def clip_params
       params.require(:clip).permit(:adress, :description) 
    end
    
    def search_params
       params.require(:clip).permit(:adress) 
    end
    
    def update_params
       params.require(:clip).permit(:description) 
    end
    
    def correct_user
        if album_to_display.nil?
            @clip=current_user.clips.find_by(id: params[:id])
        else
            @album=current_user.albums.find_by(id: album_to_display)
            @clip=@album.clips.find_by(id: params[:id])
        end
        if @clip.nil?
            flash[:danger]="You shall not pass!"
            redirect_to root_url
        end
    end
    
end

