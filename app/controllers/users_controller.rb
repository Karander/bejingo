class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy
  
  def new
    @user=User.new
  end
  
  def clips
    unless album_to_display.nil?
      album_end_display
      clip_end_display
    end
    @user=User.find(params[:id])
    @clips=@user.clips.paginate(page: params[:page], per_page: 5)
    render "users/show_clips"
  end
  
  def albums
    @album=Album.new
    @user=User.find(params[:id])
    @albums=@user.albums.paginate(page: params[:page], per_page: 5)
    unless album_to_display.nil?
      @tags=Album.find(album_to_display).tags
      @album_display=Album.find_by(id: album_to_display)
      @clips=@album_display.clips
      create_playlist(@clips)
    end
    respond_to do |format|
      format.html { render "users/show_albums" }
      format.js { toggle_autoplay }
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success]="User deleted"
    redirect_to users_url
  end
  
  def index
    @users=User.paginate(page: params[:page])
  end
  
  def show
    @user=User.find(params[:id])
    @microposts=@user.microposts.paginate(page: params[:page])
  end
  
  def create
    @user=User.new(user_params)
    if @user.save
      log_in(@user)
      flash[:success]="Account created"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
    @user=User.find(params[:id])
  end
  
  def update
    @user=User.find(params[:id])
    if @user.update_attributes(user_edit_params)
      flash[:success]="Profile updated"
      redirect_to @user
    else
      render "edit"
    end
  end
  
private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_conformation)
  end
  
  def user_edit_params
    params.require(:user).permit(:name, :email)
  end
  
## before filters ##
  
  def correct_user
    @user=User.find(params[:id])
    unless current_user?(@user)
      flash[:danger]="You have not the autorization to perform this action"
      redirect_to root_url
    end
  end
  
end
