class TagsController < ApplicationController

  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]  
  before_action :admin_user, only: [:new, :create, :edit, :update, :destroy]
  
  def new
    @tag=Tag.new
  end
  
  def create
    @tag=Tag.new(name: params[:tag][:name])
    if @tag.save
      flash[:success]="Tag created"
      redirect_to tags_path
    else
      render "new"
    end
  end
  
  def edit
    @tag=Tag.find(params[:id])
  end
  
  def update
    @tag=Tag.find(params[:id])
    if @tag.update(tag_params)
      flash[:success]="tag updated"
      redirect_to tags_path
    else
      render "edit"
    end
  end
  
  def destroy
    Tag.find(params[:id]).destroy
    flash[:success]="tag deleted"
    redirect_to tags_path
  end
  
  def index
    @tags=Tag.all
  end
  
private

  def tag_params
    params.require(:tag).permit(:name)
  end
  
end
