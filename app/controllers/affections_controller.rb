class AffectionsController < ApplicationController
    
    before_action :logged_in_user, only: [:create, :update, :destroy]

    def create
        @relation=current_user.affections.build(value: params[:value], album_id: params[:album_id])
        if @relation.save
            respond_to do |format|
                format.html { redirect_to albums_user_path(Album.find(album_to_display).user) }
                format.js
            end
        else
            redirect_to root_path
        end
    end
    
    def destroy
        current_user.affections.find_by(album_id: album_to_display).destroy
        respond_to do |format|
            format.html { redirect_to albums_user_path(Album.find(album_to_display).user) }
            format.js
        end
    end
    
    def update
        @relation=current_user.affections.find_by(album_id: album_to_display)
        if @relation.update_attribute(:value, params[:value])
            respond_to do |format|
                format.html {redirect_to albums_user_path(Album.find(album_to_display).user) }
                format.js
            end
        else
            respond_to do |format|
                format.html { redirect_to root_path }
                format.js { redirect_to root_path }
            end
        end
    end
    
end
