class TaggersController < ApplicationController
  
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: [:create, :destroy]
  
  def create
    @tag=Tag.where(name: params[:tag_id]).first
    if @tag.nil?
      flash[:danger]="Tag dose not exist"
      redirect_to albums_user_path(current_user)
    else
      @tagger=Album.find(album_to_display).taggers.build(tag_id: @tag.id)
      if @tagger.save
        flash[:success]="Tag added"
        redirect_to albums_user_path(current_user)
      else
        flash[:danger]="Ups... something went wrong"
        redirect_to albums_user_path(current_user)
      end
    end
  end
  
  def destroy
    @tagger=Tagger.find(params[:id])
    @tagger.destroy
    redirect_to albums_user_path(current_user)
  end
  
private
  
  def correct_user
    redirect_to root_url unless current_user==Album.find(album_to_display).user
  end
  
end
