class StaticPagesController < ApplicationController
  def home
    if logged_in?
      if post_to_edit.nil?
        @micropost=current_user.microposts.build
      else
        @micropost=current_user.microposts.find(post_to_edit)
      end
      @feed_items=current_user.feed.paginate(page: params[:page])
    end
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
  
end
