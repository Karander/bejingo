class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  
  private
  
  #confirms a user is logged in
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger]="Please log in tp perform this action"
      redirect_to login_url
    end
  end
  
  def admin_user
    if current_user.nil?
      redirect_to login_url
    else
      redirect_to root_url unless current_user.admin?
    end
  end
  
end
