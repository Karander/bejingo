class SearchesController < ApplicationController
    
    def new
        @search=Search.new
    end
    
    def create
        @search=Search.create(search_params)
        redirect_to @search
    end
    
    def show
        @search=Search.find(params[:id])
        @trash=@search
        @trash.destroy
    end
    
private

    def search_params
        params.require(:search).permit(:name, :min_likes, :tag)
    end
    
end
