class AlbumsController < ApplicationController
    
    before_action :logged_in_user, only: [:new, :create, :edit, :updtae, :destroy]
    before_action :correct_user, only: [:edit, :update, :destroy]
    
    def new
        @album=current_user.albums.build
    end
    
    def create
        @album=current_user.albums.build(album_params)
        if @album.save
            flash[:success]="Album created!"
            redirect_to albums_user_path(current_user)
        else
            render "new"
        end
    end
    
    def update
        @album=current_user.albums.find(params[:id])
        if @album.update(album_params)
            flash[:success]="Album updated"
            redirect_to albums_user_path(current_user)
        else
            render "edit"
        end
    end
    
    def edit
        @album=current_user.albums.find(params[:id])
    end
    
    def destroy
        @album.destroy
        flash[:success]="Album deleted"
        redirect_to request.referrer || albums_user_path(current_user)
    end
    
    def show
        @album=Album.find(params[:id])
        raise if @album==nil
        @clip=Album.find(params[:id]).clips.first
        clip_end_display
        album_end_display
        playlist_end
        unless @clip.nil?
            get_clip(@clip.adress)
        end
        get_album(params[:id])
        create_playlist(Album.find(params[:id]).clips)
        playlist_start
        autoplay("yes")
        redirect_to albums_user_path(Album.find(params[:id]).user)
    end
    
    def index
        @albums = Album.all
    end
    
private

    def album_params
        params.require(:album).permit(:title, :description)
    end
    
## before filters ##
    
    def correct_user
        @album=current_user.albums.find(params[:id])
        if @album.nil?
            flash[:danger]="You shal not pass!"
            redirect_to root_url
        end
    end
    
end
