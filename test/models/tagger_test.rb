require 'test_helper'

class TaggerTest < ActiveSupport::TestCase
  
  def setup
    @album=albums(:album_1)
    @tag=tags(:tag_2)
    @taken_tag=tags(:tag_1)
    @tagger=@album.taggers.build(tag_id: @tag.id)
  end
  
  test "should be valid" do
    assert @tagger.valid?
  end
  
  test "should require album" do
    @tagger.album_id=""
    assert_not @tagger.valid?
  end
  
  test "should require tag" do
    @tagger.tag_id=""
    assert_not @tagger.valid?
  end
  
  test "combination should be unique" do
    #see fixtures for taggers
    @tagger.tag_id=@taken_tag.id
    assert_not @tagger.valid?
  end
  
end
