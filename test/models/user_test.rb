require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user=User.new(name: "Example User", email: "example@mail.com",
                    password: "foobar", password_confirmation: "foobar")
    @album=albums(:album_1)
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "balnk or too long name should not be valid" do
    @user.name="   "
    assert_not @user.valid?
    @user.name="a"*51
    assert_not @user.valid?
  end
  
  test "balnk or too long email should not be valid" do
    @user.email="   "
    assert_not @user.valid?
    @user.email="a"*244+"@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid adresses" do
    valid_adresses=%w[user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bob@baz.cn]
    valid_adresses.each do |valid_adress|
      @user.email=valid_adress
      assert @user.valid?, "#{valid_adress.inspect} should be valid"
    end
  end
  
  test "email validation should not accept invalid adresses" do
    invalid_adresses=%w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com]
    invalid_adresses.each do |invalid_adress|
      @user.email=invalid_adress
      assert_not @user.valid?, "#{invalid_adress.inspect} should be invalid"
    end
  end
  
  test "email should be unique" do
    @duplicate_user=@user.dup
    @user.save
    @duplicate_user.email.upcase!
    assert_not @duplicate_user.valid?
  end
  
  test "should downcase email when saved" do
    mixed_email="BlaBNR@exampPLE.cOm"
    @user.email=mixed_email
    @user.save
    assert_equal mixed_email.downcase, @user.reload.email
  end
  
  test "password should not be blank" do
    @user.password=" "*6
    @user.password_confirmation=@user.password
    assert_not @user.valid?
  end
  
  test "password should be at least 6 long" do
    @user.password=@user.password_confirmation="a"*5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false if remember_digest is nil" do
    assert_not @user.authenticated?('')
  end
  
  test "dependent microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "hum bla dum")
    assert_difference "Micropost.count", -1 do
      #@user.delete (which is wrong) will not cause an error but failure!!!
      @user.destroy
    end
  end
  
  test "dependent clips should be destroyed" do
    @user.save
    @user.clips.create!(adress: "hum_bla-dum")
    assert_difference "Clip.count", -1 do
      #@user.delete (which is wrong) will not cause an error but failure!!!
      @user.destroy
    end
  end
  
  test "dependent albums should be destroyed" do
    @user.save
    assert_difference "Album.count", 1 do
      @user.albums.create!(title: "hum")
    end
    assert_difference "Album.count", -1 do
      #@user.delete (which is wrong) will not cause an error but failure!!!
      @user.destroy
    end
  end
  
  test "dependent affections should be destroyed" do
    @user.save
    assert_difference "Affection.count", 1 do
      @user.affections.create!(value: -1, album_id: @album.id)
    end
    assert_difference "Affection.count", -1 do
      @user.destroy
    end
  end
  
end
