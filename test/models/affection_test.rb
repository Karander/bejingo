require 'test_helper'

class AffectionTest < ActiveSupport::TestCase
  
  def setup
    @user=users(:michael)
    @album=albums(:album_2)
    @relation=@user.affections.build(value: 1, album_id: @album.id)
  end
  
  test "should be valid" do
    assert @relation.valid?
  end
  
  test "should require album" do
    @relation.album_id=" "
    assert_not @relation.valid?
  end
  
  test "should require value" do
    @relation.value=" "
    assert_not @relation.valid?
  end
  
  test "value should not be other than 1 or -1" do
    @relation.value=0
    assert_not @relation.valid?
  end
    
  test "should require user" do
    @relation.user_id=" "
    assert_not @relation.valid?
  end
  
end
