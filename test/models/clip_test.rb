require 'test_helper'

class ClipTest < ActiveSupport::TestCase
  
  def setup
    @user=users(:michael)
    @clip=@user.clips.build(adress: "0N_RO-jL-90")
  end
  
  test "should be valid" do
    assert @clip.valid?
  end
  
  test "should require owner" do
    @clip.owner=nil
    assert_not @clip.valid?
  end
  
  test "should get correct adress" do
    adr="tps://www.youtube.com/watch?v=_JQiEs32SqQ"
    cor_adr="_JQiEs32SqQ"
    @clip.adress=adr
    @clip.save
    assert_equal cor_adr, @clip.reload.adress
  end
    
end
