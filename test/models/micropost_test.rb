require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
  
  def setup
    @user=users(:michael)
    @micropost=@user.microposts.build(content: "Bla bla bla")
  end
  
  test "should be valid" do
    assert @micropost.valid?
  end
  
  test "should not be valid without user_id" do
    @micropost.user_id=nil
    assert_not @micropost.valid?
  end
  
  test "content should be present" do
    @micropost.content=" "
    assert_not @micropost.valid?
  end
  
  test "content should not be more than 140 char" do
    @micropost.content="a"*141
    assert_not @micropost.valid?
  end
  
  test "most recent should be first" do
    assert_equal Micropost.first, microposts(:most_recent)
  end
  
end
