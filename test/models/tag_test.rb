require 'test_helper'

class TagTest < ActiveSupport::TestCase
  
  def setup
    @tag=Tag.new(name: "Bum Bum")
    @album=albums(:album_3)
  end
  
  test "should be valid" do
    assert @tag.valid?
  end
  
  test "name should not be blank" do
    @tag.name=" "
    assert_not @tag.valid?
  end
  
  test "name should be max 15 char" do
    @tag.name="a"*16
    assert_not @tag.valid?
  end
  
  test "name should be unique and case insensitive" do
    Tag.create!(name: "bum bum")
    assert_not @tag.valid?
  end
  
  test "should downcase the name upon save" do
    @tag.save
    assert_no_match @tag.reload.name, "Bum Bum"
  end
  
  test "should destroy dependant taggers" do
    @tag.save
    assert_difference "Tagger.count", 1 do
      @album.taggers.create!(tag_id: @tag.id)
    end
    assert_difference "Tagger.count", -1 do
      @tag.destroy
    end
  end
  
end
