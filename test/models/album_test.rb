require 'test_helper'

class AlbumTest < ActiveSupport::TestCase
  
  def setup
    @tag=tags(:tag_1)
    @user=users(:michael)
    @album=@user.albums.build(title:"test-album")
  end
  
  test "should be valid" do
    assert @album.valid?
  end
  
  test "should require user and title" do
    @album.user_id=nil
    assert_not @album.valid?
    @album.user_id=@user.id
    @album.title=""
    assert_not @album.valid?
  end
  
  test "should delete dependent clips" do
    @album.save
    assert_difference "Clip.count", 1 do
      @album.clips.create!(adress: "ube.com/watch?v=Cwkej79U3ek")
    end
    assert_difference "Clip.count", -1 do
      @album.destroy
    end
  end
  
  test "should delete dependent affections" do
    @album.save
    assert_difference "Affection.count", 1 do
      @user.affections.create!(value: 1, album_id: @album.id)
    end
    assert_difference "Affection.count", -1 do
      @album.destroy
    end
  end
  
  test "should delete dependent taggers" do
    @album.save
    assert_difference "Tagger.count", 1 do
      @album.taggers.create!(tag_id: @tag.id)
    end
    assert_difference "Tagger.count", -1 do
      @album.destroy
    end
  end
  
end
