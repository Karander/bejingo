require 'test_helper'

class TagEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    log_in_as @user
    @tag=tags(:tag_1)
  end
  
  test "edit tag" do
    get tags_path
    assert_match "edit", response.body
    @new_name="testing"
    assert_no_match @new_name, response.body
    assert_match @tag.name, response.body
    patch tag_path(@tag), params: { tag: { name: @new_name } }
    assert_redirected_to tags_path
    follow_redirect!
    assert_match @new_name, @tag.reload.name
    assert_match @tag.name, response.body
  end
  
end
