require 'test_helper'

class ClipEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    @clip=clips(:Michi_clip_one)
    log_in_as(@user)
  end
  
  test "successful edit" do
    get edit_clip_path(@clip)
    assert_template "clips/edit"
    text="Hum blu kum"
    patch clip_path(@clip), params: { clip: { description: text } }
    assert_redirected_to clips_user_path(@user)
    @clip.reload
    assert_match text, @clip.description
    follow_redirect!
    assert_not flash.empty?
    assert_template "users/show_clips"
  end
  
end
