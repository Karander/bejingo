require 'test_helper'

class AffectionCreateTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    @album_owner=users(:archer)
    @album=albums(:album_3)
    log_in_as(@user)
  end
  
  test "valid creation and edit" do
    get album_path(@album)
    assert_redirected_to albums_user_path(@album_owner)
    follow_redirect!
    assert_match "btn-default", response.body
    assert_no_match "btn-success", response.body
    assert_no_match "btn-danger", response.body
    assert_difference "Affection.count", 1 do
      post affections_path(value: 1, album_id: @album.id)
    end
    assert_redirected_to albums_user_path(@album_owner)
    follow_redirect!
    assert_match "btn-default", response.body
    assert_match "btn-success", response.body
    assert_no_match "btn-danger", response.body
    patch affection_path(@album.affections.where(user_id: @user.id).first), params: {value: -1}
    assert_redirected_to albums_user_path(@album_owner)
    follow_redirect!
    assert_match "btn-default", response.body
    assert_no_match "btn-success", response.body
    assert_match "btn-danger", response.body
    assert_difference "Affection.count", -1 do
      delete affection_path(@album.affections.where(user_id: @user.id).first)
    end
    assert_redirected_to albums_user_path(@album_owner)
    follow_redirect!
    assert_match "btn-default", response.body
    assert_no_match "btn-success", response.body
    assert_no_match "btn-danger", response.body
  end
  
end
