require 'test_helper'

class ClipCreateTest < ActionDispatch::IntegrationTest
  
  def setup
    @album=albums(:album_1)
    @user=users(:michael)
    log_in_as(@user)
  end
  
  test "invalid creations" do
    get new_clip_path
    assert_no_difference "Clip.count" do
      post clips_path, params: { clip: { adress: "",
                                         description: "bum bum bum" } }
    end
    assert_template "clips/new"
    assert_match "blank", response.body
    assert_no_difference "Clip.count" do
      #clip allready in db so should refure
      post clips_path, params: { clip: { adress: "fWNaR-rxAic",
                                        description: "bum bum bum" } }
    end
    assert_template "clips/new"
    assert_no_match "blank", response.body
    assert_match "Clip allready existing", response.body
  end
  
  test "valid creations" do
    get new_clip_path
    assert_difference "Clip.count", 1 do
      post clips_path, params: { clip: { adress: "utube.com/watch?v=1cQh1ccqu8M",
                                         description: "bum bum bum" } }
    end
    assert_redirected_to clips_user_path(@user)
    follow_redirect!
    assert_template "users/show_clips"
    item=Clip.find_by(adress: "1cQh1ccqu8M")
    assert_difference "Clip.count", -1 do
      delete clip_path(item)
    end
    assert_not flash.empty?
    assert_redirected_to clips_user_path(@user)
    follow_redirect!
    get new_clip_path
    assert_difference "Clip.count", 1 do
      get_album(@album.id)
      post clips_path, params: { clip: { adress: "utube.com/watch?v=1cQh1ccqu8M",
                                         description: "bum bum bum" } }
    end
    assert_match @album.clips.count, 2
    assert_redirected_to albums_user_path(@user)
  end
  
end
