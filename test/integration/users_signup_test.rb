require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  
  test "invalid signup" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {name: "",
                                        email: "invalid.mail",
                                        password: "foo",
                                        password_confirmation: "bar"} }
      end
      assert_template "users/new"
      assert_match "blank", response.body
      assert_match "invalid", response.body
  end
  
  test "valid signup" do
    get signup_path
    assert_difference "User.count", 1 do
      post users_path, params: { user: {name: "Robert",
                                        email: "robert@mail.com",
                                        password: "password",
                                        password_confirmation: "password"} }
      end
      follow_redirect!
      assert_template "users/show"
      assert_match "Account created", response.body
      assert ts_logged_in?
  end
  
end
