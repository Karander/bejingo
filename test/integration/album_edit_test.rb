require 'test_helper'

class AlbumEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    @album=albums(:album_1)
    log_in_as(@user)
  end
  
  test "successful edit" do
    get edit_album_path(@album)
    new_title="new title"
    new_desc="new desc"
    patch album_path, params: { album: { title: new_title, description: new_desc } }
    assert_not flash.empty?
    assert_redirected_to albums_user_path(@user)
    @album.reload
    assert_match @album.title, new_title
    assert_match @album.description, new_desc
  end
  
end
