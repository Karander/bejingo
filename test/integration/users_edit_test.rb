require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              email: "foo@invalid" } }

    assert_template 'users/edit'
    assert_match "invalid", response.body
    assert_match "blank", response.body
  end
  
  test "successful edit with redirect" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    follow_redirect!
    assert_template "users/edit"
    name="Kelvin"
    email="kelvin@mail.com"
    patch user_path(@user), params: {
      user: {
        name: name,
        email: email
      }
    }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
  
end
