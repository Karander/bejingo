require 'test_helper'

class TagCreateTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    log_in_as(@user)
  end
  
  test "successful creation and destroy" do
    get tags_new_url
    assert_template "tags/new"
    assert_no_difference "Tag.count" do
      post tags_path, params: { tag: { name: "a" } }
    end
    assert_template "tags/new"
    assert_match "short", response.body
    @tag_name="test_tag"
    assert_difference "Tag.count", 1 do
      post tags_path, params: { tag: {name: @tag_name } }
    end
    assert_redirected_to tags_path
    assert_not flash.empty?
    follow_redirect!
    assert_template "tags/index"
    assert_match "delete", response.body
    assert_match @tag_name, response.body
    assert_difference "Tag.count", -1 do
      delete tag_path(Tag.find_by(name: @tag_name))
    end
    assert_redirected_to tags_path
    follow_redirect!
    assert_no_match @tag_name, response.body
  end
  
end
