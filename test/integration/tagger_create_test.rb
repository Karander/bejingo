require 'test_helper'

class TaggerCreateTest < ActionDispatch::IntegrationTest
  
  def setup
    @user_1=users(:michael)
    @user_2=users(:archer)
    @tag=tags(:tag_2)
    @tagger=taggers(:tagger_1)
    @album=albums(:album_1)
  end
  
  test "should require correct user" do
    log_in_as(@user_2)
    get album_path(@album)
    assert_redirected_to albums_user_path(@user_1)
    follow_redirect!
    assert_no_difference "Tagger.count" do
      delete tagger_path(@tagger)
    end
    assert_redirected_to root_url
    get album_path(@album)
    assert_redirected_to albums_user_path(@user_1)
    follow_redirect!
    assert_no_difference "Tagger.count" do
      post taggers_path, params: { tag_id: @tag.name }
    end
    assert_redirected_to root_url
  end
  
  test "destroy and create" do
    log_in_as(@user_1)
    get album_path(@album)
    assert_redirected_to albums_user_path(@user_1)
    follow_redirect!
    assert_difference "Tagger.count", -1 do
      delete tagger_path(@tagger)
    end
    assert_redirected_to albums_user_path(@user_1)
    follow_redirect!
    assert_difference "Tagger.count", 1 do
      post taggers_path, params: { tag_id: @tag.name }
    end
    assert_redirected_to albums_user_path(@user_1)
  end
  
end
