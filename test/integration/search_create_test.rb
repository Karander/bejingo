require 'test_helper'

class SearchCreateTest < ActionDispatch::IntegrationTest
  
  def setup
    @album1=albums(:album_1)
    @album2=albums(:album_2)
    @album3=albums(:album_3)
  end
  
  test "should get all for empty form" do
    get new_search_path
    assert_template "searches/new"
    post searches_path, params: { search: { name: "", tag: "", min_likes: ""} }
    follow_redirect!
    assert_template "searches/show"
    assert_match @album1.title, response.body
    assert_match @album2.title, response.body
    assert_match @album3.title, response.body
    assert_equal Search.count, 0
  end
  
  test "should get non for non existing params" do
    get new_search_path
    assert_template "searches/new"
    post searches_path, params: { search: { name: "aaaaaaa", tag: "", min_likes: ""} }
    follow_redirect!
    assert_template "searches/show"
    assert_no_match @album1.title, response.body
    assert_no_match @album2.title, response.body
    assert_no_match @album3.title, response.body
    get new_search_path
    assert_template "searches/new"
    post searches_path, params: { search: { name: "", tag: "aaaaaaaaa", min_likes: ""} }
    follow_redirect!
    assert_template "searches/show"
    assert_no_match @album1.title, response.body
    assert_no_match @album2.title, response.body
    assert_no_match @album3.title, response.body
    assert_equal Search.count, 0
  end
  
  test "should get correct results" do
    get new_search_path
    assert_template "searches/new"
    post searches_path, params: { search: { name: "3", tag: "", min_likes: ""} }
    follow_redirect!
    assert_template "searches/show"
    assert_no_match @album1.title, response.body
    assert_no_match @album2.title, response.body
    assert_match @album3.title, response.body
    get new_search_path
    assert_template "searches/new"
    post searches_path, params: { search: { name: "", tag: "test_3", min_likes: ""} }
    follow_redirect!
    assert_template "searches/show"
    assert_match @album1.title, response.body
    assert_match @album2.title, response.body
    assert_match @album3.title, response.body
    get new_search_path
    assert_template "searches/new"
    post searches_path, params: { search: { name: "2", tag: "test_2", min_likes: ""} }
    follow_redirect!
    assert_template "searches/show"
    assert_no_match @album1.title, response.body
    assert_match @album2.title, response.body
    assert_no_match @album3.title, response.body
    assert_equal Search.count, 0
  end
  
end
