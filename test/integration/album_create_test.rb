require 'test_helper'

class AlbumCreateTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    log_in_as(@user)
  end
  
  test "valid creation" do
    get new_album_path
    assert_difference "Album.count", 1 do
      post albums_path, params: { album: { title: "new test album", description: " " } }
    end
    assert_not flash.empty?
    assert_redirected_to albums_user_path(@user)
    item=Album.find_by(title: "new test album")
    assert_difference "Album.count", -1 do
      delete album_path(item)
    end
    assert_redirected_to albums_user_path(@user)
  end
  
  test "invalid creation" do
    get new_album_path
    assert_no_difference "Album.count" do
      #title must be present
      post albums_path, params: { album: { title: " " } }
    end
    assert_template "albums/new"
  end
  
end
