ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper
  include SessionsHelper
  
  def ts_logged_in?
    !session[:user_id].nil?
  end
  
  # Loggin in a user in a controller test.
  def log_in_as(user)
    session[:user_id]=user.id
  end

  # Add more helper methods to be used by all tests here...
end
  
class ActionDispatch::IntegrationTest
  
  # Logging in a user in an integration test.
  def log_in_as(user, password: "password", remember_me: "1")
    post login_path params: {
      session: {
        email: user.email,
        password: password,
        remember_me: remember_me
      }
    }
  end
    
end
