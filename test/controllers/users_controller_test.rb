require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    @other_user=users(:archer)
  end
  
  test "should get new" do
    get signup_path
    assert_response :success
  end
  
  test "should redirect edit if not logged in" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_path
  end
  
  test "should redirect update if not logged in" do
    patch user_path(@user), params: {
      user: {
        name: "Kelvin",
        email: "kelvin@mail.com"
      }
    }
    assert_not flash.empty?
    assert_redirected_to login_path
    @user.reload
    assert_no_match @user.name, "Kelvin"
    assert_no_match @user.email, "kelvin@mail.com"
  end
  
  test "should redirect edit if wrong user logged in" do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect update if wrong user logged in" do
    log_in_as(@other_user)
    patch user_path(@user), params: {
      user: {
        name: "Kelvin",
        email: "kelvin@mail.com"
      }
    }
    assert_not flash.empty?
    assert_redirected_to root_url
    @user.reload
    assert_no_match @user.name, "Kelvin"
    assert_no_match @user.email, "kelvin@mail.com"
  end
  
  test "should redirect destroy if not logged in" do
    assert_no_difference "User.count" do
      delete user_path(@other_user)
    end
    assert_redirected_to login_url
    assert_not flash.empty?
  end
  
  test "should redirect destroy if wrong user" do
    log_in_as(@other_user)
    assert_no_difference "User.count" do
      delete user_path(@user)
    end
    assert_redirected_to root_url
  end

end
