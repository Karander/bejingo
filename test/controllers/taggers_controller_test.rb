require 'test_helper'

class TaggersControllerTest < ActionDispatch::IntegrationTest
    
    def setup
        @album=albums(:album_1)
        @user=users(:michael)
        @tagger=taggers(:tagger_1)
    end

    test "should redirect if not logged in" do
        assert_no_difference "Tagger.count" do
            post taggers_path, params: { tag_id: "piano" }
        end
        assert_redirected_to login_url
        assert_no_difference "Tagger.count" do
            delete tagger_path(@tagger)
        end
        assert_redirected_to login_url
    end
    
    test "should inform if tag not available" do
        log_in_as(@user)
        get album_path(@album)
        assert_redirected_to albums_user_path(@user)
        follow_redirect!
        assert_no_difference "Tagger.count" do
            post taggers_path, params: { tag_id: "non_existing" }
        end
        assert_redirected_to albums_user_path(@user)
        follow_redirect!
        assert_match "not exist", response.body
    end
    
end
