require 'test_helper'

class TagsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    @no_admin=users(:archer)
    @tag=tags(:tag_1)
  end
  
  test "should have access for admin" do
    log_in_as(@user)
    get tags_new_url
    assert_response :success
    get tags_path
    assert_response :success
    get edit_tag_path(@tag)
    assert_response :success
  end
  
  test "should redirect if not logged in" do
    get tags_new_url
    assert_redirected_to login_url
    get edit_tag_path(@tag)
    assert_redirected_to login_url
  end
  
  test "should have access for all" do
    get tags_path
    assert_response :success
  end
  
  test "should redirect if not admin expect for index" do
    log_in_as(@no_admin)
    get tags_new_url
    assert_redirected_to root_url
    get tags_path
    assert_response :success
    get edit_tag_path(@tag)
    assert_redirected_to root_url
    @new_name="testing"
    patch tag_path(@tag), params: { name: @new_name }
    assert_redirected_to root_url
    assert_no_match @new_name, @tag.reload.name
    assert_no_difference "Tag.count" do
      delete tag_path(@tag)
    end
    assert_redirected_to root_url
    assert_no_difference "Tag.count" do
      post tags_path, params: { name: "new tag" }
    end
  end

end
