require 'test_helper'

class AffectionsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    @album=albums(:album_2)
    @aff=affections(:affection_1)
  end
  
  test "should redirect create if not logged in" do
    assert_no_difference "Affection.count" do
      post affections_path(value: 1, album_id: @album.id)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect patch if not logged in" do
    patch affection_path(@aff), params: { value: -1 }
    assert_redirected_to login_url
    @aff.reload
    assert_not_equal -1, @aff.value
  end
  
  test "should redirect destroy if not logged in" do
    assert_no_difference "Affection.count" do
      delete affection_path(@aff)
    end
    assert_redirected_to login_url
  end

end
