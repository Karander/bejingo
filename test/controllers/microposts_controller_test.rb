require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @micropost=microposts(:orange)
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference "Micropost.count" do
      post microposts_path, params: { micropost: { content: "bla bla"}}
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference "Micropost.count" do
      delete micropost_path(@micropost)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect update when not logged in" do
    new_text="muh muh"
    patch micropost_path(@micropost), params: { micropost: { content: new_text}}
    assert_redirected_to login_url
    @micropost.reload
    assert_not_equal new_text, @micropost.content
  end
  
  test "should redirect edit when not logged in" do
    get edit_micropost_path(@micropost)
    assert_redirected_to login_url
  end
  
end
