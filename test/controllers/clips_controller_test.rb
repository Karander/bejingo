require 'test_helper'

class ClipsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user=users(:michael)
    @other_user=users(:archer)
    @clip=clips(:Michi_clip_one)
  end
  
##logged in user##

  test "should redirect show to users_clips" do
    get clip_path(@clip)
    assert_redirected_to clips_user_path(@user)
  end
  
  test "should redirect destroy if not logged in" do
    assert_no_difference "Clip.count" do
      delete clip_path(@clip)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect edit if not logged in" do
    get edit_clip_path(@clip)
    assert_redirected_to login_url
  end
  
  test "should redirect create if not logged in" do
    assert_no_difference "Clip.count" do
      post clips_path, params: { clip: { adress: "2vjPBrBU-TM" } }
    end
    assert_redirected_to login_url
  end
  
  test "should redirect update if not logged in" do
    text="lupe"
    patch clip_path(@clip), params: { clip: { description: text } }
    assert_redirected_to login_url
    @clip.reload
    assert_not_equal text, @clip.adress
  end
  
##correct user
  
  test "should redirect destroy if wrong user" do
    log_in_as(@other_user)
    assert_no_difference "Clip.count" do
      delete clip_path(@clip)
    end
    assert_redirected_to root_url
  end
  
  test "should redirect edit if wrong user" do
    log_in_as(@other_user)
    get edit_clip_path(@clip)
    assert_redirected_to root_url
  end
  
  test "should redirect update if wrong user" do
    log_in_as(@other_user)
    text="lupe"
    patch clip_path(@clip), params: { clip: { description: text } }
    assert_redirected_to root_url
    @clip.reload
    assert_not_equal text, @clip.description
  end
  
end
