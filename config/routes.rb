Rails.application.routes.draw do
  
  get 'taggers/new'

  get 'tags/new'

  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  delete "/logout", to: "sessions#destroy"
  
  get '/signup', to: "users#new"
  
  get "/about", to: "static_pages#about"
  get '/help', to: "static_pages#help"
  get '/contact', to: "static_pages#contact"

  root "static_pages#home"
  
  resources :users do
    member do
      get :clips, :albums
    end
  end
  resources :microposts, only: [:create, :update, :destroy, :edit]
  resources :clips, only: [:create, :update, :edit, :destroy, :show, :new]
  resources :albums, only: [:create, :update, :edit, :destroy, :show, :new]
  resources :affections, only: [:create, :update, :destroy]
  resources :tags, only: [:create, :update, :destroy, :new, :edit, :index]
  resources :taggers, only: [:create, :destroy]
  resources :searches, only: [:new, :show, :create]
  
end
