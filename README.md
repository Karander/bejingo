# README

Things to solve:

-testing clips with albums as owners, see
    http://stackoverflow.com/questions/39466611/rails-testing-how-to-pass-a-hash-to-a-controller
    for more details
-uniquenes validation failing even though the test passes, see
    http://stackoverflow.com/questions/39468424/uniqueness-validation-for-polymorphic-model-fails
    (solved with a different approach yet still interesting)
-SOLVED: "unless album_to_display==album.id" in "albums/album" dose not work || album_to_display is not integer by default
-FIX STATIC-PAGES-CONTROLLER-TEST