class CreateAffections < ActiveRecord::Migration[5.0]
  def change
    create_table :affections do |t|
      t.integer :value
      t.references :user, foreign_key: true
      t.references :album, foreign_key: true

      t.timestamps
    end
    add_index :affections, [:user_id, :album_id], unique: true
  end
end
