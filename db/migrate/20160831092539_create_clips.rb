class CreateClips < ActiveRecord::Migration[5.0]
  def change
    create_table :clips do |t|
      t.string :adress
      t.references :owner, polymorphic: true
      t.text :description
      t.timestamps
    end
    add_index :clips, [:owner_id, :owner_type, :adress], unique: true
  end
end
