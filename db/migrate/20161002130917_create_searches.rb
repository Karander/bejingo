class CreateSearches < ActiveRecord::Migration[5.0]
  def change
    create_table :searches do |t|
      t.string :name
      t.string :tag
      t.integer :min_likes

      t.timestamps
    end
  end
end
