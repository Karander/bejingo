User.create!(
    name: "Example User",
    email: "example@railstutorial.org",
    password: "foobar",
    password_confirmation: "foobar",
    admin: true)
    
99.times do |n|
    name=Faker::Name.name
    email="example-#{n+1}@railstutorial.org"
    password="password"
    User.create!(
        name: name,
        email: email,
        password: password,
        password_confirmation: password)
end

user1=User.first
user1.clips.create!(adress: "Q0VRj2uw9L0", description: "test etst testing")
user1.albums.create!(title: "test album")
album=user1.albums.first
album.clips.create!(adress: "w.youtube.com/watch?v=XAAp_luluo0", description: "test best testing")

users=User.order(:created_at).take(6)
50.times do
    content=Faker::Lorem.sentence(5)
    users.each do |user|
        user.microposts.create!(content: content)
    end
end

users=User.order(:created_at).take(7)
users.each do |user|
    unless user.id==1
        user.affections.create(value: 1, album_id: album.id)
    end
end

tags=["mix", "pop", "rap", "rock", "metal", "country", "instrumental", "piano", "classic"]
tags.each do |tag|
    Tag.create!(name: tag)
end